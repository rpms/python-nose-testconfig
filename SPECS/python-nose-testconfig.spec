%global srcname nose-testconfig

%if 0%{?rhel} > 7
# Disable python2 build by default
%bcond_with python2
%else
%bcond_without python2
%endif

Name:           python-nose-testconfig
Version:        0.10
Release:        10%{?dist}
Summary:        Test configuration plugin for nosetests

License:        ASL 2.0
URL:            https://bitbucket.org/jnoller/nose-testconfig/
Source0:        https://pypi.python.org/packages/source/n/%{srcname}/%{srcname}-%{version}.tar.gz

# Upstream does not include the license, which is required for redistribution
# See https://bitbucket.org/jnoller/nose-testconfig/issue/10/
Source1:        LICENSE-2.0

BuildArch:      noarch

%description
nose-testconfig is a plugin to the nose test framework which provides a
faculty for passing test-specific (or test-run specific) configuration data
to the tests being executed.

Currently configuration files in the following formats are supported:

- YAML (via PyYAML <http://pypi.python.org/pypi/PyYAML/>)
- INI (via ConfigParser <http://docs.python.org/lib/module-ConfigParser.html>)
- Pure Python (via Exec)
- JSON (via JSON <http://docs.python.org/library/json.html>)

%if %{with python2}
%package -n python2-%{srcname}
Summary:        Test configuration plugin for nosetests for Python2
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-setuptools

Requires:       python2-nose
Requires:       python2-pyyaml

%description -n python2-%{srcname}
nose-testconfig is a plugin to the nose test framework which provides a
faculty for passing test-specific (or test-run specific) configuration data
to the tests being executed.

Currently configuration files in the following formats are supported:

- YAML (via PyYAML <http://pypi.python.org/pypi/PyYAML/>)
- INI (via ConfigParser <http://docs.python.org/lib/module-ConfigParser.html>)
- Pure Python (via Exec)
- JSON (via JSON <http://docs.python.org/library/json.html>)

This is the Python 2 version of the package.
%endif # with python2

%package -n python3-%{srcname}
Summary:        Test configuration plugin for nosetests for Python3
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       python3-nose
Requires:       python3-PyYAML

%description -n python3-%{srcname}
nose-testconfig is a plugin to the nose test framework which provides a
faculty for passing test-specific (or test-run specific) configuration data
to the tests being executed.

Currently configuration files in the following formats are supported:

- YAML (via PyYAML <http://pypi.python.org/pypi/PyYAML/>)
- INI (via ConfigParser <http://docs.python.org/lib/module-ConfigParser.html>)
- Pure Python (via Exec)
- JSON (via JSON <http://docs.python.org/library/json.html>)

This is the Python 3 version of the package.

%prep
%autosetup -n %{srcname}-%{version}
rm -rf nose_testconfig.egg-info
cp %{SOURCE1} .

%build
%if %{with python2}
%py2_build
%endif # with python2
%py3_build

%install
%if %{with python2}
%py2_install
%endif # with python2
%py3_install

%if %{with python2}
%files -n python2-%{srcname}
%license LICENSE-2.0
%doc ACKS TODO docs/index.txt
%{python2_sitelib}/testconfig.py*
%{python2_sitelib}/nose_testconfig-%{version}-*.egg-info
%endif # with python2

%files -n python3-%{srcname}
%license LICENSE-2.0
%doc ACKS TODO docs/index.txt
%{python3_sitelib}/testconfig.py*
%{python3_sitelib}/__pycache__/testconfig.*
%{python3_sitelib}/nose_testconfig-%{version}-*.egg-info

%changelog
* Wed Jun 06 2018 Charalampos Stratakis <cstratak@redhat.com> - 0.10-10
- Conditionalize the python2 subpackage

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.10-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Fri Jan 26 2018 Iryna Shcherbina <ishcherb@redhat.com> - 0.10-8
- Update Python 2 dependency declarations to new packaging standards
  (See https://fedoraproject.org/wiki/FinalizingFedoraSwitchtoPython3)

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.10-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.10-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.10-5
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10-4
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.10-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.10-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Mon Oct  5 2015 David Shea <dshea@redhat.com> - 0.10-1
- Update to upstream version 0.10, which adds support for multiple configs
- Change to the new packaging guildelines which renames python-nose-testconfig to python2-nose-testconfig

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jan 28 2015 David Shea <dshea@redhat.com> - 0.9-4
- Use %%license for the license file

* Tue Aug 12 2014 David Shea <dshea@redhat.com> - 0.9-3
- Added a comment about the inclusion of an external copy of the ASL

* Mon Aug 11 2014 David Shea <dshea@redhat.com> - 0.9-2
- Include a copy of the Apache Software License, 2.0.

* Thu Apr 10 2014 David Shea <dshea@redhat.com> - 0.9-1
- Initial package
